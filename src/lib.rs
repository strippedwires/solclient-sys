#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use std::fmt;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

impl fmt::Display for solClient_returnCode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            _ => write!(f, "{:?}", self),
        }
    }
}

impl fmt::Display for solClient_subCode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            _ => write!(f, "{:?}", self),
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn try_init() {
        unsafe {
            let mut result = solClient_initialize(
                solClient_log_level::SOLCLIENT_LOG_NOTICE,
                std::ptr::null_mut(),
            );
            assert!(result == 0);

            let mut context_p: solClient_opaqueContext_pt = std::ptr::null_mut();
            let mut contextFuncInfo = solClient_context_createFuncInfo {
                regFdInfo: solClient_context_createRegisterFdFuncInfo {
                    regFdFunc_p: None,
                    unregFdFunc_p: None,
                    user_p: std::ptr::null_mut(),
                },
            };

            let props = _solClient_contextPropsDefaultWithCreateThread.as_mut_ptr();

            result = solClient_context_create(
                props,
                &mut context_p,
                &mut contextFuncInfo,
                std::mem::size_of::<solClient_context_createRegisterFdFuncInfo>(),
            );

            assert!(result == 0);
        }
    }
}
