# solclient-sys

Rust [bindgen](https://github.com/rust-lang/rust-bindgen) wrapper for
[Solace Systems'](http://dev.solace.com) C API.

This crate is just the minimal bindings for the library.

See the [Solace API
Documentation](https://docs.solace.com/API-Developer-Online-Ref-Documentation/c/index.html)
for details.

## Building

This crate requires Solace's C API libraries for your target platform; once you
have unpacked the tarball, set the SOLACE_ROOT variable to the path to the
directory. For example:

```
export SOLACE_ROOT=$HOME/solclient-7.7.1.4
```

Note that due to the use of 128bit floats in the API, this crate uses the
RustTarget::Stable_1_33 target for bindgen.
