extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    let solace_root = match env::var("SOLACE_ROOT") {
        Ok(dir) => PathBuf::from(dir),
        Err(e) => {
            panic!("Environment variable SOLACE_ROOT could not be read: {}", e);
        }
    };

    println!("cargo:rustc-link-lib=static=solclient");
    println!("cargo:rustc-link-lib=ssl");
    println!("cargo:rustc-link-lib=crypto");
    println!("cargo:rustc-link-lib=gssapi_krb5");
    println!("cargo:rustc-link-search=/usr/local/opt/openssl@1.1/lib");
    println!(
        "cargo:rustc-link-search={}/lib",
        solace_root.to_str().unwrap()
    );

    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .clang_arg(format!("-I{}/include", solace_root.to_str().unwrap()))
        .clang_arg("-DSOLCLIENT_EXCLUDE_DEPRECATED")
        .clang_arg("-DPROVIDE_LOG_UTILITIES")
        .clang_arg("-DSOLCLIENT_CONST_PROPERTIES")
        .clang_arg("-g")
        .default_enum_style(bindgen::EnumVariation::Rust {
            non_exhaustive: true,
        })
        .rustfmt_bindings(true)
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());

    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
